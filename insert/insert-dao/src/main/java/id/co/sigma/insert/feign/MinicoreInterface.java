package id.co.sigma.insert.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import id.co.sigma.insert.model.Account;
import id.co.sigma.insert.model.Customer;
import id.co.sigma.insert.response.MessageResponse;

@FeignClient("minicore")
public interface MinicoreInterface {
	
	@RequestMapping(path = "/minicore/customer/get/all")
	public List<Customer> getAll();
	
	@RequestMapping(path = "/minicore/customer/add")
	public MessageResponse add(@RequestBody Customer customer);
	
	@RequestMapping(path = "/minicore/customer/update")
	public MessageResponse updateCostumer(@RequestBody Customer customer);
	
	@RequestMapping(path = "/minicore/customer/delete/{id}")
	public List<Customer> deleteById (@PathVariable("id") Long id);
	
	@RequestMapping(path = "/minicore/customer/get/{id}")
	public Customer getByIdCustomer(@PathVariable("id") Long id);
	
	//account
	
	@RequestMapping(path = "/minicore/account/get/all")
	public List<Account> getAllAccounts();
	
	@RequestMapping(path = "/minicore/account/add")
	public MessageResponse add(@RequestBody Account account);
	
	@RequestMapping(path = "minicore/account/update")
	public MessageResponse updateAccount(@RequestBody Account account);
	
	@RequestMapping(path = "minicore/account/delete/{id}")
	public List<Account> deleteByIdAccount(@PathVariable("id") Long id);
	
	@RequestMapping(path = "minicore/account/get/{id}")
	public List<Account> inquiryAll(@PathVariable("id") Long id);
	
	@RequestMapping(value = "/minicore/webservice/listuser", method = {RequestMethod.POST})
	List listUser();
	
}