package id.co.sigma.insert.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.sigma.insert.feign.MinicoreInterface;
import id.co.sigma.insert.model.Account;
import id.co.sigma.insert.response.MessageResponse;

@Service
public class AccountService {
	
	@Autowired
	private MinicoreInterface minicoreInterface;
	
	public List<Account> findAll() {
		return minicoreInterface.getAllAccounts();
	}
	
	public MessageResponse save(Account account) {
		return minicoreInterface.add(account);
	}
	
	public MessageResponse update(Account account) {
		return minicoreInterface.updateAccount(account);
	}
	
	public List<Account> deleteById(Long id) {
		return minicoreInterface.deleteByIdAccount(id);
	}
	
	public List<Account> inquiryAll(Long id){
		return minicoreInterface.inquiryAll(id);
	}
	
}
