package id.co.sigma.insert.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.sigma.insert.model.Customer;
import id.co.sigma.insert.response.MessageResponse;
import id.co.sigma.insert.service.CustomerService;

@RestController
public class CustomerController {

	@Autowired
	private CustomerService service;
	
	@RequestMapping(path = "/customer/get/all")
	public List<Customer> getAll() {
		return service.findAll();
	}

	@RequestMapping(path = "/customer/update")
	public MessageResponse update(@RequestBody Customer customer) {
		return service.update(customer);
	}
	
	@RequestMapping(path = "/customer/delete/{id}")
	public List<Customer> deleteById(@PathVariable("id") Long id) {
		service.deleteByIdcus(id);
		return getAll();
	}
	
	@RequestMapping(path = "/customer/add")
	public MessageResponse add(@RequestBody Customer customer) {
		return service.save(customer);
	}
	
	@RequestMapping(path = "/customer/get/{id}")
	public Customer getByIdCustomer(Long id) {
		return service.findByIdCustomer(id);
	}
}
