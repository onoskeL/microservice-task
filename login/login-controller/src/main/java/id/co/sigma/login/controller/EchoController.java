package id.co.sigma.login.controller;


import org.springframework.web.bind.annotation.*;

import id.co.sigma.login.request.EchoRequest;
import id.co.sigma.login.response.BaseResponse;

import java.io.IOException;


@RestController
public class EchoController{

	public static final String TERMINAL_ECHO_PATH = "/webservice/echo**";

	@RequestMapping(value = TERMINAL_ECHO_PATH, method = RequestMethod.POST)
	public @ResponseBody
	BaseResponse echo(@RequestBody EchoRequest request) throws IOException {
		BaseResponse response = new BaseResponse();
		if (request.getRequestCode().equals("ECHO")) {
			response.setResponseCode("00");
			response.setResponseMessage("SUCCESS");
		} else {
			response.setResponseCode("01");
			response.setResponseMessage("FAILED");
		}
		return response;
	}
}