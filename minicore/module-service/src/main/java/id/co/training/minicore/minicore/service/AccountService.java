package id.co.training.minicore.minicore.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.training.minicore.minicore.model.Account;
import id.co.training.minicore.minicore.repository.AccountRepository;

@Service
public class AccountService {
	
	@Autowired
	private AccountRepository repository;
	
	public List<Account> findAll(){
		return repository.findAll();
	}
	
	public Account findById(Long id){
		return repository.findById(id).get();
	}
	
	public void save(Account account) {
		repository.save(account);
	}
	
	public void delete(Long id) {
		Account account = repository.findById(id).get();
		repository.delete(account);
	}
	
	public Account findByNumber(String number) {
		return repository.findByNumber(number);
	}

	
}
