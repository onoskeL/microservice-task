package id.co.training.minicore.minicore.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import id.co.training.minicore.minicore.model.Customer;
import id.co.training.minicore.minicore.repository.CustomerRepository;

@Service
@CacheConfig(cacheNames = "customerService")
public class CustomerService {

	@Autowired
	private CustomerRepository repository;
	
	
	@Cacheable(value = "minicore.user.findAll", unless = "#result == null")
	public List<Customer> findAll(){
		return repository.findAll();
	}
	
	@Cacheable(value = "minicore.user.findById", unless = "#result == null")
	public Customer findById(Long id){
		return repository.findById(id).get();
	}
	
	
	@Caching(evict = {
			@CacheEvict(value = "minicore.user.findAll", allEntries = true, beforeInvocation = true),
			@CacheEvict(value = "minicore.findById", allEntries = true, beforeInvocation = true)
	})
	public void save(Customer customer) {
		repository.save(customer);
	}
	
	@Caching(evict = {
			@CacheEvict(value = "minicore.user.findAll", allEntries = true, beforeInvocation = true),
			@CacheEvict(value = "minicore.findById", allEntries = true, beforeInvocation = true)
	})
	public void delete(Long id) {
		Customer customer = repository.findById(id).get();
		repository.delete(customer);
	}
}
